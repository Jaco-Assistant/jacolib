import os

from jacolib import utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"


# ==================================================================================================


def test_skill_prefix() -> None:
    sp = utils.skill_to_prefix("Skill-Riddles")
    assert sp == "skill_riddles"

    sp = utils.skill_to_prefix("Jaco-MusicMaster-2")
    assert sp == "jaco_musicmaster_2"

    sp = utils.skill_to_prefix("Skill_für-$$")
    assert sp == "skill_fr"


# ==================================================================================================


def test_load_global_config() -> None:
    utils.set_repo_path(file_path + "data/")
    config = utils.load_global_config()
    assert config["language"] == "es"
    assert config["speech_transcription_timeout"] == 7.5
    assert config["a_nested_config"]["not_shallow"]["val1"] == "a"
    assert config["a_nested_config"]["not_shallow"]["val2"] == "c"


# ==================================================================================================


def test_load_skill_config() -> None:
    skill_path = file_path + "data/skills/skills/testskill"
    config = utils.load_skill_config(skill_path)
    assert config["user"]["language"] == "de"
    assert config["system"]["has_action"] is True

    tops = ["get_riddle", "check_riddle"]
    assert config["system"]["topics_read"] == tops
    tops = ["Jaco/Skills/SayText"]
    assert config["system"]["topics_write"] == tops


# ==================================================================================================


def test_load_architecture() -> None:
    utils.set_repo_path(file_path + "data/")
    arch = utils.load_architecture()
    assert arch == "amd64"


# ==================================================================================================


def test_get_skills_with_paths() -> None:
    skills_with_paths = utils.get_skills_with_paths()
    skill, path = skills_with_paths[0]
    assert skill == "testskill"
    assert "tests/data/skills/skills/testskill" in path


# ==================================================================================================


def test_seconds_to_hours() -> None:
    assert utils.seconds_to_hours(123.45) == "0:02:03"
    assert utils.seconds_to_hours(3636.63) == "1:00:36"
