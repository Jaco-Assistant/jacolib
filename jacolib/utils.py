import json
import os
import re
import shutil
from typing import Optional

import yaml

# ==================================================================================================

repo_path: Optional[str] = None
allowed_architectures = ["arm64", "amd64", "armhf"]


# ==================================================================================================


def set_repo_path(path: str) -> None:
    global repo_path
    repo_path = path


# ==================================================================================================


def get_repo_path() -> str:
    if repo_path is not None:
        return repo_path
    raise RuntimeError("Set this path first")


# ==================================================================================================


def load_json_file(path: str) -> dict:
    with open(path, "r", encoding="utf-8") as file:
        content: dict = json.load(file)
    return content


# ==================================================================================================


def load_yaml_file(path: str) -> dict:
    with open(path, "r", encoding="utf-8") as file:
        content: dict = yaml.load(file, Loader=yaml.SafeLoader)
    return content


# ==================================================================================================


def merge_dicts(source: dict, target: dict) -> dict:
    """Overwrite values in target dict recursively with values from source dict"""

    for key, value in source.items():
        if isinstance(value, dict):
            if key in target:
                merged = merge_dicts(value, target[key])
            else:
                merged = value
            target[key] = merged
        else:
            target[key] = value

    return target


# ==================================================================================================


def load_config(config_dir: str, config_name: str) -> dict:
    """Load config file with either '.yaml' or '.template.yaml' file endings"""

    # Load user config if existing
    path = os.path.join(config_dir, config_name + ".yaml")
    if os.path.exists(path):
        user_config = load_yaml_file(path)
    else:
        user_config = {}

    # Load template file
    path = os.path.join(config_dir, config_name + ".template.yaml")
    tmpl_config = load_yaml_file(path)

    config = merge_dicts(user_config, tmpl_config)
    return config


# ==================================================================================================


def load_global_config() -> dict:
    path = get_repo_path() + "userdata/config/"
    config = load_config(path, "global_config")
    return config


# ==================================================================================================


def load_skill_config(skill_path: str) -> dict:
    config = load_config(skill_path, "config")
    return config


# ==================================================================================================


def load_architecture() -> str:
    """Check and return the container architecture to be used"""

    config = load_global_config()
    arch: str = config["architecture"]
    arch = arch.lower()

    if arch not in allowed_architectures:
        msg = "Your config has an error, choose one of those architectures: "
        msg = msg + str(allowed_architectures)
        raise ValueError(msg)

    return arch


# ==================================================================================================


def get_skills_with_paths() -> list:
    """Return a list of tuples of all the installed skills with skill name and directory path"""

    skills_dir = get_repo_path() + "skills/skills/"
    if os.path.exists(skills_dir):
        skills = [s for s in os.listdir(skills_dir) if os.path.isdir(skills_dir + s)]
        skills_with_paths = [(s, skills_dir + s) for s in skills]
        return skills_with_paths
    return []


# ==================================================================================================


def skill_to_prefix(skill_name: str) -> str:
    """Converts skill name to snake_case and removes special characters"""

    skill_prefix = skill_name.lower()
    skill_prefix = skill_prefix.replace("-", "_")
    skill_prefix = re.sub(r"[^a-z0-9_]", "", skill_prefix)

    if skill_prefix.startswith("_"):
        skill_prefix = skill_prefix[1:]
    if skill_prefix.endswith("_"):
        skill_prefix = skill_prefix[:-1]

    return skill_prefix


# ==================================================================================================


def empty_with_ignore(path: str) -> None:
    """Deletes and recreates a directory. Adds a gitignore file to ignore directory contents"""

    if os.path.isdir(path):
        try:
            shutil.rmtree(path)
            os.mkdir(path)
        except OSError as err:
            if len(os.listdir(path)) == 0:
                # In the special case that this directory is mounted as volume it can't be deleted
                # But it will be emptied so the error can be ignored
                # Example case: Building the nlu model for Barrista-Jaco benchmark
                pass
            else:
                raise err
    else:
        os.mkdir(path)

    with open(path + ".gitignore", "w+", encoding="utf-8") as file:
        file.write("*\n!.gitignore\n")


# ==================================================================================================


def seconds_to_hours(secs: float) -> str:
    secs = int(secs)
    m, s = divmod(secs, 60)
    h, m = divmod(m, 60)
    t = "{:d}:{:02d}:{:02d}".format(h, m, s)
    return t
