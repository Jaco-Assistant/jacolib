import os
import random
import time
from typing import Any, Callable, Optional

from paho.mqtt.client import Client, MQTTMessage

from . import comm_tools, extra_tools, utils

# ==================================================================================================


class Assistant:
    def __init__(self, repo_path: str):
        self.callbacks: dict = {}
        self.repo_path = repo_path
        utils.set_repo_path(self.repo_path + "../../../")
        comm_tools.set_skill_path(self.repo_path)

        self.config = utils.load_skill_config(self.repo_path)
        self.global_config = utils.load_global_config()

        skill = os.path.basename(os.path.normpath(self.repo_path))
        self.skill_prefix = utils.skill_to_prefix(skill)

        topic_suf = "".join(x.title() for x in self.skill_prefix.split("_"))
        self.say_topic = "Jaco/Skills/SayText" + topic_suf

        self.talks = self.load_talks()
        self.mqtt_client = comm_tools.connect_mqtt_client(
            self.global_config, self.on_connect, self.on_message
        )

        ua_topic = "Jaco/Skills/UserAnswer"
        if ua_topic in self.config["system"]["topics_read"]:
            self.add_topic_callback(ua_topic, self.user_answer_callback)
        self.ua_answer: Optional[dict]

    # ==============================================================================================

    def run(self) -> None:
        """Start to listen to the skill's topics. Normally this is an endless blocking call"""
        self.mqtt_client.loop_forever()

    # ==============================================================================================

    def get_config(self) -> dict:
        return self.config

    def get_global_config(self) -> dict:
        return self.global_config

    def get_talks(self) -> dict:
        return self.talks

    def get_random_talk(self, key: str) -> str:
        result: str = random.choice(self.talks[key])
        return result

    # ==============================================================================================

    @staticmethod
    def extract_entities(message: dict, key: str, values_only: bool = True) -> list:
        """Keep this as class method too, to be consistent with other functions"""
        return extract_entities(message, key, values_only)

    # ==============================================================================================

    def load_talks(self) -> dict:
        """Load the talks dictionary in the user's language"""

        path = "{}dialog/talks/{}.json"
        path = path.format(self.repo_path, self.global_config["language"].lower())

        talks = {}
        if os.path.exists(path):
            talks = utils.load_json_file(path)
        return talks

    # ==============================================================================================

    def add_topic_callback(self, topic: str, callback: Callable) -> None:
        """Add callback if topic is listed in config file, else raise error"""

        if topic not in self.config["system"]["topics_read"]:
            raise ValueError("Topic {} is not listed in the config!".format(topic))

        if not topic.startswith("Jaco/"):
            topic = self.skill_prefix + "-" + topic
            topic = extra_tools.intent_to_topic(topic)

        try:
            _ = comm_tools.get_encryptor(topic)
            self.callbacks[topic] = callback
            self.mqtt_client.subscribe(topic)
        except LookupError as e:
            print("Is this topic listed in the config file?")
            raise e

    # ==============================================================================================

    def publish(self, payload: dict) -> None:
        """Encrypt and publish the message on the skills own SayText topic"""

        msg = comm_tools.encrypt_msg(payload, self.say_topic)
        self.mqtt_client.publish(self.say_topic, msg)

    # ==============================================================================================

    def publish_answer(self, result_sentence: str, satellite: str) -> None:
        """Send a simple message which will be read to the user"""
        payload = {
            "data": result_sentence,
            "satellite": satellite,
            "timestamp": time.time(),
        }
        self.publish(payload)
        print("Sent answer to the user:", payload)

    # ==============================================================================================

    def publish_question(
        self, question_sentence: str, question_intents: list, satellite: str
    ) -> dict:
        """Send a question and wait for the answer.
        Returns an empty dict if none of the given intents were detected."""

        payload = {
            "data": question_sentence,
            "question_intents": question_intents,
            "satellite": satellite,
            "timestamp": time.time(),
        }
        self.publish(payload)
        self.mqtt_client.loop_write()
        print("Sent a question to the user:", payload)

        # Wait until the dialog-manager returns an answer
        self.ua_answer = None
        while self.ua_answer is None:
            time.sleep(0.1)

        print("Received an answer from the user:", self.ua_answer)
        return self.ua_answer

    # ==============================================================================================

    def user_answer_callback(self, message: dict) -> None:
        """Decrypt and set the detected answer of the user"""

        if message["topic"] != "Jaco/Intents/IntentNotRecognized":
            pld = message["payload"].encode()
            msg = comm_tools.decrypt_msg(pld, message["topic"])
        else:
            msg = {}
        self.ua_answer = msg

    # ==============================================================================================

    def on_connect(self, client: Client) -> None:
        pass

    def on_message(
        self,
        client: Client,  # pylint: disable=unused-argument
        userdata: Any,  # pylint: disable=unused-argument
        msg: MQTTMessage,
    ) -> None:
        print("Received message on topic:", msg.topic)
        payload = comm_tools.decrypt_msg(msg.payload, msg.topic)

        cb = self.callbacks[msg.topic]
        cb(payload)


# ==================================================================================================


def extract_entities(message: dict, key: str, values_only: bool = True) -> list:
    """Extract a list of entities from a nlu message.
    If there are multiple occurrences of the same key,
    returns a list of all the matching values"""

    results = []
    if "entities" in message:
        for e in message["entities"]:
            if e["entity"] == key:
                if values_only:
                    results.append(e["value"])
                else:
                    # Return the complete dictionary, including the roles
                    results.append(e)
    return results
