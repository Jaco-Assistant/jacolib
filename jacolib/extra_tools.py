import base64
import re
from typing import Optional

# ==================================================================================================


def intent_to_topic(intent: Optional[str]) -> str:
    """Converts intent name to SkillName/EntityName and adds topic prefix"""
    topic_prefix = "Jaco/Intents/"

    topic: str
    if intent is not None:
        parts = intent.lower().split("-")
        parts = ["".join(x.title() for x in part.split("_")) for part in parts]
        topic = "/".join(parts)
        topic = topic_prefix + topic
    else:
        topic = topic_prefix + "IntentNotRecognized"

    return topic


# ==================================================================================================


def topic_to_intent(topic: str) -> str:
    """Converts topic name to skill_name-entity_name and removes topic prefix"""
    topic_prefix = "Jaco/Intents/"

    if topic.startswith(topic_prefix):
        topic = topic[len(topic_prefix) :]
        parts = topic.split("/")
        parts = [re.sub(r"(?<!^)(?=[A-Z])", "_", part) for part in parts]
        intent = "-".join(parts)
        intent = intent.lower()
    else:
        raise ValueError("This is not a topic for intents:", topic)

    return intent


# ==================================================================================================


def wav_to_base64(path: str) -> str:
    with open(path, "rb") as file:
        content = file.read()
    encoded: str = base64.b64encode(content).decode()
    return encoded
