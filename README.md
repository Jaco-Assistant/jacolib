# JacoLib

This is a collection of tools used across different modules of Jaco.

[![pipeline status](https://gitlab.com/Jaco-Assistant/jacolib/badges/master/pipeline.svg)](https://gitlab.com/Jaco-Assistant/jacolib/-/commits/master)
[![coverage report](https://gitlab.com/Jaco-Assistant/jacolib/badges/master/coverage.svg)](https://gitlab.com/Jaco-Assistant/jacolib/-/commits/master)
[![code style black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![code complexity](https://gitlab.com/Jaco-Assistant/jacolib/-/jobs/artifacts/master/raw/badges/rcc.svg?job=analysis)](https://gitlab.com/Jaco-Assistant/jacolib/-/commits/master)

<br>

## Testing

Install from a directory or branch:

```bash
pip3 install --user -e .
pip3 install --upgrade git+https://gitlab.com/Jaco-Assistant/jacolib@my_branch
```

Build container with testing tools:

```bash
docker build -f tests/Containerfile -t testing_jacolib .

docker run --network host --rm \
  --volume `pwd`/:/jacolib/ \
  -it testing_jacolib
```

Run unit tests:

```bash
pip3 install --user -e /jacolib/
pytest /jacolib/ --cov=jacolib -v -s
```

For syntax tests, check out the steps in the [gitlab-ci](.gitlab-ci.yml#L53) file.
